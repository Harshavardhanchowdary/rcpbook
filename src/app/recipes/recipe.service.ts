import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredients.modal';
import { ShopingListService } from '../shoping-list/shopinglist.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {
  recipeChanged = new EventEmitter<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe(
      '--Korean BBQ chicken burgers recipe',
      'Guaranteed to impress guests, these BBQ chicken burgers have been given a Korean kick thanks to a tangy dark soy sauce and ginger marinade.',
      'https://realfood.tesco.com/media/images/57-KOREAN-CHICKEN-BURGER-LGH-b71ac44e-ef11-4e1e-b82d-efbe74e816af-0-1400x919.jpg',
      [
        new Ingredient('Spring onion', 1),
        new Ingredient('Garlic clove', 2),
        new Ingredient('Soft mixed seed deli rolls', 4),
        new Ingredient('Little Gem lettuce', 1),
        new Ingredient('Chicken thighs', 4),
        new Ingredient('SKorean ssamjang sauce', 1),
      ]
    ),
    new Recipe(
      '--New potato, chicken and red onion skewers recipe',
      'Jamie says: "Harissa is a storecupboard hero – an easy shortcut to big flavour, it brings a smoky warmth to these oven-roasted skewers.',
      'https://realfood.tesco.com/media/images/1400x919-New-potato-skewers-recipe-bbc4cde3-6fa3-48cb-9fbf-aad509507d25-0-1400x919.jpg',
      [
        new Ingredient('Jersey Royal', 10),
        new Ingredient('Garlic cloves', 2),
        new Ingredient('Large red onion', 1),
        new Ingredient('Chicken thigh fillets', 5),
        new Ingredient('Little Gem lettuces', 2),
        new Ingredient('Natural yogurt', 1),
      ]
    ),
  ];

  constructor(private ingeridentListService: ShopingListService) {}

  setRecipes(recipesFetched: Recipe[]) {
    this.recipes = recipesFetched;
    this.recipeChanged.emit(this.recipes.slice());
  }

  getRecipies() {
    return this.recipes.slice();
  }

  getRecipieById(index: number) {
    this.recipeChanged.emit(this.recipes.slice());
    return this.recipes.slice()[index];
  }

  recipeSelected = new EventEmitter<Recipe>();

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.ingeridentListService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.emit(this.recipes.slice());
  }

  updaterecipe(index: number, recipe: Recipe) {
    this.recipes[index] = recipe;
    this.recipeChanged.emit(this.recipes.slice());

    console.log(recipe);
    console.log(this.recipes);
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipeChanged.emit(this.recipes.slice());
  }
}
