import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RecipeService } from '../recipes/recipe.service';
import { Recipe } from '../recipes/recipe.model';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(private http: HttpClient, private recipeService: RecipeService) {}

  storeRecipiesToFirebase() {
    const recipies = this.recipeService.getRecipies();
    this.http
      .put(
        'https://recp-book-default-rtdb.firebaseio.com/recipes.json',
        recipies
      )
      .subscribe((res) => {
        console.log(res);
      });
  }

  fetchRecipiesFromFirebase() {
    this.http
      .get<Recipe[]>(
        'https://recp-book-default-rtdb.firebaseio.com/recipes.json'
      )
      .subscribe((res) => {
        this.recipeService.setRecipes(res);
      });
  }
}
