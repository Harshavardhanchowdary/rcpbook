import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredients.modal';
import { ShopingListService } from './shopinglist.service';

@Component({
  selector: 'app-shoping-list',
  templateUrl: './shoping-list.component.html',
  styleUrls: ['./shoping-list.component.css'],
})
export class ShopingListComponent implements OnInit {
  ingrediens: Ingredient[];
  constructor(private shopingListService: ShopingListService) {}

  ngOnInit(): void {
    this.ingrediens = this.shopingListService.getShopingList();
    this.shopingListService.ingredientsChanged.subscribe(
      (ingredients: Ingredient[]) => {
        this.ingrediens = ingredients;
      }
    );
  }

  onIngredientSelect(i: number) {
    this.shopingListService.startedEditing.emit(i);
  }

  
}
