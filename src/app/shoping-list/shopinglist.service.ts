import { EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredients.modal';

export class ShopingListService {
  ingredientsChanged = new EventEmitter<Ingredient[]>();
  startedEditing = new EventEmitter<number>();

  private ingrediens: Ingredient[] = [
    new Ingredient('Apples', 22),
    new Ingredient('Tomatos', 33),
  ];

  getShopingList() {
    return this.ingrediens.slice();
  }

  getIngredient(index: number) {
    return this.ingrediens[index];
  }

  addIngredient(ingredient: Ingredient) {
    this.ingrediens.push(ingredient);
    this.ingredientsChanged.emit(this.ingrediens.slice());
  }

  addIngredients(ingredients: Ingredient[]) {
    this.ingrediens.push(...ingredients);
    this.ingredientsChanged.emit(this.ingrediens.slice());
  }

  updateIngredient(index: number, newIngredient: Ingredient) {
    this.ingrediens[index] = newIngredient;
    this.ingredientsChanged.emit(this.ingrediens.slice());
  }

  deleteIngredient(index: number) {
    this.ingrediens.splice(index, 1);
    this.ingredientsChanged.emit(this.ingrediens.slice());
  }
}
