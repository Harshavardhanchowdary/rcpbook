import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Ingredient } from 'src/app/shared/ingredients.modal';
import { ShopingListService } from '../shopinglist.service';

@Component({
  selector: 'app-shoping-edit',
  templateUrl: './shoping-edit.component.html',
  styleUrls: ['./shoping-edit.component.css'],
})
export class ShopingEditComponent implements OnInit {
  @ViewChild('f', { static: false }) shoppingListForm: NgForm;
  editingMode = false;
  editingItemindex: number;
  editedItem: Ingredient;
  constructor(private shopingListService: ShopingListService) {}

  ngOnInit(): void {
    this.shopingListService.startedEditing.subscribe((index: number) => {
      this.editingItemindex = index;
      this.editingMode = true;
      this.editedItem = this.shopingListService.getIngredient(index);
      this.shoppingListForm.setValue({
        name: this.editedItem.name,
        amount: this.editedItem.amount,
      });
    });
  }

  onAddIngredient(form: NgForm) {
    console.log(form);
    const value = form.value;
    const newIng = new Ingredient(value.name, value.amount);
    if (this.editingMode) {
      this.shopingListService.updateIngredient(this.editingItemindex, newIng);
    } else {
      this.shopingListService.addIngredient(newIng);
    }
    this.editingMode = false;
    form.reset();
  }

  onClear() {
    this.shoppingListForm.reset();
    this.editingMode = false;
  }

  onDelete() {
    this.shopingListService.deleteIngredient(this.editingItemindex);
    this.editingMode = false;
    this.shoppingListForm.reset();
  }
}
