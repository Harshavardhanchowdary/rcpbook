import { Component, Injectable } from '@angular/core';
import { RecipeService } from '../recipes/recipe.service';
import { DataStorageService } from '../shared/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  providers: [RecipeService, DataStorageService],
})
@Injectable({
  providedIn: 'root',
})
export class HeaderComponent {
  constructor(private dataStorageService: DataStorageService) {}

  onSaveData() {
    this.dataStorageService.storeRecipiesToFirebase();
  }

  onFetchData() {
    this.dataStorageService.fetchRecipiesFromFirebase();
  }
}
